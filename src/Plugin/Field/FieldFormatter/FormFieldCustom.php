<?php

  namespace Drupal\form_field_formatter\Plugin\Field\FieldFormatter;

  use Drupal\Core\Field\FieldItemListInterface;
  use \Drupal\Core\Field\FormatterBase;
  use Drupal\Core\Form\FormStateInterface;
  use Drupal\Core\Render\Element\Form;

  /**
   * Plugin implementation of the "form_field_custom" formatter
   *
   * @FieldFormatter(
   *   id = "form_field_custom",
   *   label = "Form Field Custom",
   *   field_types = {
   *     "boolean",
   *     "changed",
   *     "created",
   *     "decimal",
   *     "email",
   *     "entity_reference",
   *     "float",
   *     "integer",
   *     "language",
   *     "map",
   *     "password",
   *     "string",
   *     "string_long",
   *     "timestamp",
   *     "uri",
   *     "uuid",
   *     "comment",
   *     "datetime",
   *     "file",
   *     "image",
   *     "link",
   *     "list_float",
   *     "list_integer",
   *     "list_string",
   *     "path",
   *     "telephone",
   *     "text",
   *     "text_long",
   *     "text_with_summary"
   *   }
   * )
   */
  class FormFieldCustom extends FormatterBase {
    /**
     * {@inheritdoc}
     *
     * @param \Drupal\Core\Field\FieldItemListInterface $items
     * @param string $langcode
     */
    public function viewElements(FieldItemListInterface $items, $langcode) {
      // TODO: Implement viewElements() method.
      $elements = [
        '#theme' => 'form_field_formatter',
        '#items' => [],
        '#fff_wrap_prefix' => $this->getSetting('fff_wrap_prefix'),
        '#fff_wrap_suffix' => $this->getSetting('fff_wrap_suffix'),
      ];

      // Get settings from settingsForm
      $classes = $this->getSetting('fff_classes');
      $id      = $this->getSetting('fff_id');
      $prefix  = $this->getSetting('fff_prefix');
      $suffix  = $this->getSetting('fff_suffix');

      foreach ($items as $delta => $item) {
        // Set to call the hook_theme.
        $elements['#items'][$delta] = [
          'item'  => $item->view(),
        ];

        if ($classes)
          $elements['#items'][$delta]['fff_classes'] = explode(',', str_replace(' ', '', $classes));
        if ($id)
          $elements['#items'][$delta]['fff_id'] = $id;
        if ($prefix)
          $elements['#items'][$delta]['fff_prefix'] = $prefix;
        if ($suffix)
          $elements['#items'][$delta]['fff_suffix'] = $suffix;
      }

      return $elements;
    }

    /**
     * @param array $form
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *
     * @return array
     */
    public function settingsForm(array $form, FormStateInterface $form_state) {
      $form = parent::settingsForm($form, $form_state);

      // HTML Classes
      $form['fff_classes'] = [
        '#type' => 'textfield',
        '#default_value' => $this->getSetting('fff_classes'),
        '#attributes' => [
          'placeholder' => 'HTML Classes',
        ],
        '#description' => $this->t('Write HTML class (if multiples, separing them by comma) which will added to the form field')
      ];
      // HTML ID
      $form['fff_id'] = [
        '#type' => 'textfield',
        '#default_value' => $this->getSetting('fff_id'),
        '#attributes' => [
          'placeholder' => 'HTML ID',
        ],
        '#description' => $this->t('Write an ID value without the #')
      ];

      /**
       * Add items wrapper.
       */
      $form['fff_wrap_prefix'] = [
        '#type' => 'textfield',
        '#default_value' => $this->getSetting('fff_wrap_prefix'),
        '#attributes' => [
          'placeholder' => $this->t('HTML Wrap prefix'),
        ],
        '#description' => $this->t('Add Wrap prefix HTML tag that will rendered before all your field items. (es: &lt;div class=&quot;carousel&quot;&gt;)')
      ];
      $form['fff_wrap_suffix'] = [
        '#type' => 'textfield',
        '#default_value' => $this->getSetting('fff_wrap_suffix'),
        '#attributes' => [
          'placeholder' => $this->t('HTML Wrap suffix'),
        ],
        '#description' => $this->t('Add Wrap suffix HTML tag that will rendered after all your field items. (es: &lt;/div&gt;)')
      ];

      /**
       * Add prefix and suffix field.
       */
      $form['fff_prefix'] = [
        '#type' => 'textfield',
        '#default_value' => $this->getSetting('fff_prefix'),
        '#attributes' => [
          'placeholder' => $this->t('HTML prefix'),
        ],
        '#description' => $this->t('Add prefix HTML tag that will rendered before your form field')
      ];
      $form['fff_suffix'] = [
        '#type' => 'textfield',
        '#default_value' => $this->getSetting('fff_suffix'),
        '#attributes' => [
          'placeholder' => $this->t('HTML suffix'),
        ],
        '#description' => $this->t('Add suffix HTML tag that will rendered after your form field')
      ];

      return $form;
    }

    public static function defaultSettings() {
      return [
        'fff_classes' => '',
        'fff_id' => '',
        'fff_wrap_prefix' => '',
        'fff_wrap_suffix' => '',
        'fff_prefix' => '',
        'fff_suffix' => ''
      ] + parent::defaultSettings();
    }

    public function settingsSummary() {
      $wrap_pre = $this->getSetting('fff_wrap_prefix') == true ? $this->getSetting('fff_wrap_prefix') : 'NO_WRAP_PREFIX';
      $wrap_suf = $this->getSetting('fff_wrap_suffix') == true ? $this->getSetting('fff_wrap_suffix') : 'NO_WRAP_SUFFIX';

      $pre = $this->getSetting('fff_prefix') == true ? " " . $this->getSetting('fff_prefix') : 'NO_PREFIX';
      $suf = $this->getSetting('fff_suffix') == true ? " " . $this->getSetting('fff_suffix') : 'NO_SUFFIX';
      return [
        '#wrap_prefix' => $wrap_pre,
        '#prefix' => $pre,
        '#items'  => '{{ items }}',
        '#suffix' => $suf,
        '#wrap_suffix' => $wrap_suf,
      ];
    }
  }
